---
menu:
    main:
        weight: 1
        
title: "Home" 
params:
  author: "Juliette Dubois"
  siteHeading: "a header title"
---

Please see my new personal page here: <a href=https://juliettedubois.gitlab.io/website> https://juliettedubois.gitlab.io/website </a> 
